package bsuir;

import java.util.ArrayList;
import java.util.Scanner;

class Menu {

  Menu() {
    ArrayListUtils arrayListUtils = new ArrayListUtils();
    arrayListUtils.arrayList = new ArrayList<>();
    Scanner scanner = new Scanner(System.in);
    while (true) {
      printMenu();
      switch (scanner.nextLine()) {
        case "1":
          System.out.println("Input element to add: ");
          arrayListUtils.addElementToList(scanner.nextLine());
          break;
        case "2":
          System.out.println("Input element to delete: ");
          arrayListUtils.removeElementFromList(scanner.nextLine());
          break;
        case "3":
          System.out.println("Input element to learn duplicates: ");
          arrayListUtils.countEqualElements(scanner.nextLine());
          break;
        case "4":
          arrayListUtils.reverseAllStringsInList();
          System.out.println("All strings in list have been reversed");
          break;
        case "5":
          arrayListUtils.printSymbolStatistics();
          break;
        case "6":
          System.out.println("Input substring: ");
          arrayListUtils.findSubstring(scanner.nextLine());
          break;
        case "7":
          arrayListUtils.readStringsFromFile();
          break;
        case "8":
          System.out.println("Input first index: ");
          int firstIndex = scanner.nextInt();
          System.out.println("Input second index: ");
          int secondIndex = scanner.nextInt();
          arrayListUtils.compareInnerObjects(firstIndex, secondIndex);
          break;
        case "9":arrayListUtils.countLengthsAndSort();
          break;
        case "10":
          arrayListUtils.showCurrentList();
          break;
        case "11":
          System.exit(-1);
        default:
          System.out.println("Choose from [1-10], please");
      }
    }
  }

  private void printMenu() {
    System.out.println("\nMenu:");
    System.out.println("1.Add element to list");
    System.out.println("2.Delete element from list");
    System.out.println("3.Count equal elements");
    System.out.println("4.Reverse all elements");
    System.out.println("5.Show symbol statistics");
    System.out.println("6.Find substring");
    System.out.println("7.Initialize list from file");
    System.out.println("8.Compare two strings");
    System.out.println("9.Get string lengths and sort result");
    System.out.println("10.Set static size of list");
    System.out.println("11.Show current list");
    System.out.println("12.Exit");
  }

}
