package bsuir;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class ArrayListUtils extends ArrayList {

  ArrayList<String> arrayList;

  void addElementToList(String object) {
    arrayList.add(object);
  }


  void removeElementFromList(String object) {
    arrayList.remove(object);
  }

  void showCurrentList() {
    System.out.println(arrayList);
  }

  void countEqualElements(String supposedDuplicate) {
    Map<String, Long> result = arrayList.stream()
        .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
    System.out.println(
        "Number of " + supposedDuplicate + " duplicates: " + result.get(supposedDuplicate));
  }

  void reverseAllStringsInList() {
    for (int i = 0; i < arrayList.size(); i++) {
      String element = arrayList.get(i);
      StringBuilder stringBuilder = new StringBuilder(element).reverse();
      arrayList.remove(i);
      arrayList.add(i, stringBuilder.toString());
    }
  }

  void printSymbolStatistics() {
    char[] alphabet = "abcdefghijklmnopqrstuvwxyz1234567890".toCharArray();
    for (String element : arrayList) {
      System.out.println("-------Statistics for \'" + element + "\'-------");
      for (char character : alphabet) {
        StringBuilder result = new StringBuilder();
        long counter = element.chars().filter(ch -> ch == character).count();
        if (counter == 0) {
          continue;
        }
        result.append("\'").append(character).append("\'").append(" was met ").append(counter)
            .append(" times.\n");
        System.out.println(result);
      }
    }
  }

  void findSubstring(String substring) {
    for (String element : arrayList) {
      if (element.contains(substring)) {
        System.out.println(substring + " was met in " + element);
      } else {
        System.out.println("There is no such substring in list");
      }
    }
  }

  void readStringsFromFile() {
    arrayList.clear();
    try (Stream<String> stream = Files.lines(Paths.get("D:/strings.txt"), StandardCharsets.UTF_8)) {
      stream.forEach(s -> arrayList.add(s));
    } catch (IOException e) {
      e.printStackTrace();
    }
    System.out.println("Array list was cleared and filled with strings from file.");
  }

  void compareInnerObjects(int firstIndex, int secondIndex) {
    try {
      if (arrayList.get(firstIndex).equals(arrayList.get(secondIndex))) {
        System.out.println("Elements with entered indexes are equal");
      } else {
        System.out.println("Elements are not equal");
      }
    } catch (IndexOutOfBoundsException e) {
      System.out.println("Error! There is no element with such index!");
    }
  }

  void countLengthsAndSort() {
    List<String> linkedList = new LinkedList<>(arrayList);
    linkedList.sort(Comparator.naturalOrder());
    for (String element : linkedList) {
      System.out.println("Length of \'" +element+"\' is " + element.length());
    }
  }

}


